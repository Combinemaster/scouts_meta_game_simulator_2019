﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class node : MonoBehaviour
{
	public GameObject northNode;
	public GameObject southNode;
	public GameObject westNode;
	public GameObject eastNode;

	public bool explored;

	public string ownedBy = metagame_script.NONE;
	public string areaName = "";

	private int fortLevel = 0;
	private int gateLevel = 0;

	private metagame_script metaGameScript;

	// Start is called before the first frame update
	void Start()
	{
		metaGameScript = GameObject.FindGameObjectWithTag("metagame_script").GetComponent<metagame_script>();
		if (ownedBy.Equals(metagame_script.NONE))
		{
			gameObject.GetComponent<SpriteRenderer>().color = Color.grey;
		}
	}

	public void updateNodeFortificationLevel(int fortLevel, int gateLevel) {
		this.fortLevel = fortLevel;
		this.gateLevel = gateLevel;
	}

	public void upgradeFort() {
		if (fortLevel == 0 && gateLevel == 0) {
			fortLevel = 1;
			gateLevel = 1;
		} else {
			fortLevel++;
		}
	}

	public void upgradeGate() {
		if (fortLevel == 0 && gateLevel == 0) {
			fortLevel = 1;
			gateLevel = 1;
		}
		else {
			gateLevel++;
		}
	}

	public int getCurrentGateLevel() {
		return gateLevel;
	}

	public int getCurrentFortLevel() {
		return fortLevel;
	}

	public int returnFortStrengthLevelAIAttack() {
		switch (fortLevel) {
			case 1: return 3;
			case 2: return 6;
			case 3: return 9;
			case 4: return 12;
			default: return 0;
		}
	}

	public int returnGateStrengthLevelAIAttack() {
		switch (gateLevel) {
			case 1: return 3;
			case 2: return 6;
			case 3: return 9;
			case 4: return 12;
			default: return 0;
		}
	}

	public string returnFortStrengthLevelPlayerAttack() {
		switch (fortLevel) {
			case 1: return "Commander's Lair";
			case 2: return "Commander's Lair, Inner Gate";
			case 3: return "Commander's Lair, Inner Gate, Outer Gate";
			case 4: return "temp no option 4";
			default: return "No fort level";
		}
	}

	public string returnGateStrengthLevelPlayerAttack() {
		switch (gateLevel) {
			case 1: return "2 Hits";
			case 2: return "3 Hits";
			case 3: return "4 Hits";
			case 4: return "temp no option 4";
			default: return "No gate level";
		}
	}

	public bool isFortLevelMax() {
		return fortLevel == 4;
	}

	public bool isGateLevelMax() {
		return gateLevel == 4;
	}

	public bool isFortMoreThanLevel1() {
		return fortLevel > 1;
	}

	public int calculateDefenderBonus() {
		return fortLevel * 5 + gateLevel * 5;
	}

	public void setOwnership(string newOwner)
	{
		ownedBy = newOwner;
	}

	public bool isFortified() {
		return fortLevel >= 1 && gateLevel >= 1;
	}

	public void resetNodeGateFortLevel() {
		fortLevel = 0;
		gateLevel = 0;
	}

	public node navigateToNextNode() {
		explored = false;

		List<node> exploreableNodes = new List<node>();

		if (northNode != null) {
			exploreableNodes.Add(northNode.GetComponent<node>());
		}

		if (southNode != null) {
			exploreableNodes.Add(southNode.GetComponent<node>());
		}

		if (westNode != null) {
			exploreableNodes.Add(westNode.GetComponent<node>());
		}

		if (eastNode != null) {
			exploreableNodes.Add(eastNode.GetComponent<node>());
		}

		if (exploreableNodes.Count == 0) {
			return this;
		}

		int randomDirection = Random.Range(0, exploreableNodes.Count);
		return exploreableNodes[randomDirection];
	}

	public bool nearbyNodesFortified() {
		if ((northNode != null && northNode.GetComponent<node>().isFortified() && northNode.GetComponent<node>().ownedBy.Equals(ownedBy)) ||
			(southNode != null && southNode.GetComponent<node>().isFortified() && southNode.GetComponent<node>().ownedBy.Equals(ownedBy)) ||
			(westNode != null && westNode.GetComponent<node>().isFortified() && westNode.GetComponent<node>().ownedBy.Equals(ownedBy)) ||
			(eastNode != null && eastNode.GetComponent<node>().isFortified() && eastNode.GetComponent<node>().ownedBy.Equals(ownedBy))) {
			return true;
		}

		return false;
	}

	private void OnMouseDown()
	{
		if (metaGameScript.isPlayersTurn() && metaGameScript.isPlayerMoving()) {
			List<node> exploreableNodes = new List<node>();

			if (northNode != null) {
				exploreableNodes.Add(northNode.GetComponent<node>());
			}

			if (southNode != null) {
				exploreableNodes.Add(southNode.GetComponent<node>());
			}

			if (westNode != null) {
				exploreableNodes.Add(westNode.GetComponent<node>());
			}

			if (eastNode != null) {
				exploreableNodes.Add(eastNode.GetComponent<node>());
			}

			foreach (node item in exploreableNodes) {
				if (item.ownedBy.Equals(metaGameScript.getPlayerFaction())) {
					if (ownedBy.Equals(metagame_script.NONE)) {
						ownedBy = metaGameScript.getPlayerFaction();
						gameObject.GetComponent<SpriteRenderer>().color = Color.red;
						metaGameScript.onPlayerMoveFinished();
						return;
					}
					else if (!ownedBy.Equals(metaGameScript.getPlayerFaction())) {
						metaGameScript.onPlayerAttackAction(this, "Viking", true);
						return;
					}
				}
			}
		} else if (metaGameScript.isPlayersTurn() && metaGameScript.isPlayerFortifying() && ownedBy.Equals(metaGameScript.getPlayerFaction())) {
			metaGameScript.onPlayerNodeSelected(this);
		}

		metaGameScript.populateNodeInfoPanel(ownedBy, fortLevel.ToString(), gateLevel.ToString(), areaName);
	}
}
