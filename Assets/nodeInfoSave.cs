﻿using System.Collections.Generic;

[System.Serializable]
public class nodeInfoSave
{
	public List<string> nodeOwnershipList = new List<string>();
	public List<int> nodeGateLevelList = new List<int>();
	public List<int> nodeFortLevelList = new List<int>();
	public Dictionary<string, faction> factionList = new Dictionary<string, faction>();
	public int roundCount = 0;
}
