﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class faction
{
	public int levelTwoBotsAvailable = 0;
	public int levelThreeBotsAvailable = 0;
	string factionName = "";
	SerializableColor factionColor = new SerializableColor();
	public int requistionAvaliable = 0;
	public bool playerOwned = false;

	private int levelTwoBotsInUse = 0;
	private int levelThreeBotsInUse = 0;

	public faction(GameObject factionHomeBase, bool playerOwned, string factionName, SerializableColor factionColor) {
		this.playerOwned = playerOwned;
		this.factionName = factionName;
		this.factionColor = factionColor;

		factionHomeBase.GetComponent<node>().setOwnership(factionName);
		factionHomeBase.GetComponent<SpriteRenderer>().color = factionColor.Color;
	}

	public void incrementLevelTwoBotInUse() {
		levelTwoBotsInUse++;
	}

	public void incrementLevelThreeBotInUse() {
		levelThreeBotsInUse++;
	}

	public void factionLoss() {
		levelTwoBotsAvailable -= levelTwoBotsInUse;
		levelThreeBotsAvailable -= levelThreeBotsInUse;

		levelTwoBotsInUse = 0;
		levelThreeBotsInUse = 0;
	}

	public void factionWin() {
		levelTwoBotsInUse = 0;
		levelThreeBotsInUse = 0;
	}

	public bool hasLevelTwoBots() {
		return levelTwoBotsAvailable > 0 && levelTwoBotsAvailable > levelTwoBotsInUse;
	}

	public bool hasLevelThreeBots() {
		return levelThreeBotsAvailable > 0 && levelThreeBotsAvailable > levelThreeBotsInUse;
	}

	public string getFactionNameUI() {
		return char.ToUpper(factionName.Substring(0, 1).ToCharArray()[0]) + factionName.Substring(1).ToLower();
	}

	public string getFactionName() {
		return factionName;
	}

	public SerializableColor getFactionColor() {
		return factionColor;
	}
}
