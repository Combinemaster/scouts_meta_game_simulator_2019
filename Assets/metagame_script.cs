﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

//Class that serves as a script for managing the metagame, this class will roll for actions that the AI would want to take based on a flow chart.
public class metagame_script : MonoBehaviour
{
	//General
	public Button saveGameButton;
	public Button loadGameButton;

	//Player actions
	public Button playerMoveButton;
	public Button playerRecruitButton;
	public Button playerFortifyButton;
	public Button playerEndTurnButton;
	public Button playerForitfyCancelButton;
	public Button playerForitfyDoneButton;

	public Button[] gateLevels = new Button[4];
	public Button[] fortLevels = new Button[4];

	public Text playerReq;
	public Text playerLevelTwoBots;
	public Text playerLevelThreeBots;
	public Text playerReqFortify;

	public Button playerRecruitLevelTwoBotButton;
	public Button playerRecruitLevelThreeBotButton;

	public GameObject ctaRecruitActionPanel;
	public GameObject ctaFortifyActionPanel;

	//Attack Manual Action
	public Text attackActionManualSlotOneAILevel;
	public Text attackActionManualSlotTwoAILevel;
	public Text attackActionManualSlotThreeAILevel;
	public Text attackActionManualSlotFourAILevel;

	public Text attackActionManualPlayerBot2Avaliable;
	public Text attackActionManualPlayerBot3Avaliable;
	public Text attackActionManualPlayerReqAvaliable;
	public Text attackFortLevel;
	public Text attackGateLevel;

	public Text attackActionManualMapName;
	public Text attackActionManualFactionName;
	public Text attackActionManualDefendingFaction;
	public Text attackActionManualBattleType;

	public Text attackActionAiSiegeRamHealth;
	public Text attackActionAiSiegeExtraTickets;
	public Text attackActionAiSiegePikeDamage;

	public InputField playerBotLevelSlot1;
	public InputField playerBotLevelSlot2;
	public InputField playerBotLevelSlot3;
	public InputField playerBotLevelSlot4;

	public Dropdown playerExtraTickets;
	public Dropdown playerRamHealth;
	public Dropdown playerPikeDamage;

	public Text playerBotLevelErrorMessage;

	public GameObject attackActionManualPanel;
	public GameObject attackActionManualFullScreenPanel;
	public GameObject attackActionAiSiegeDecisionsPanel;
	public GameObject attackActionPlayerSiegeDecisionsPanel;

	public Button attackActionConfirmBotLevel;
	public Button attackActionConfirmSiege;
	public Button attackActionPlayerWin;
	public Button attackActionPlayerLoss;

	//node info panel
	public Text nodeInfoOwnedBy;
	public Text nodeInfoGateLevel;
	public Text nodeInfoFortLevel;
	public Text nodeInfoAreaName;

	//Action Log
	public GameObject aiActionTextPrefab;
	public GameObject logPanel;
	public GameObject actionOutput;
	public ScrollRect scrollableView;
	public Text roundCounter;

	//Confirm action dialog
	public GameObject ctaConfirmPanel;
	public Text ctaText;
	public int actionType;

	//AI Faction Homes (Can change with conquest)
	public GameObject knightHomeBase;
	public GameObject samuraiHomeBase;
	public GameObject chineseHomeBase;
	public GameObject vikingHomeBase;

	//Areas To Save
	public GameObject[] factionAreas = new GameObject[4];

	//Faction data 
	private Dictionary<string, faction> factionList;

	//Player battle data
	private faction attackingFaction;
	private faction defendingFaction;
	private node nodeUnderAttack;
	private bool isPlayerAttacking;
	private bool isAIAttackingInSiege;

	private bool playerMoving = false;
	private bool playerFortifying = false;
	private node fortifyingNode = null;

	private const string MOVE = "Move";
	private const string FORTIFY = "Fortify";
	private const string RECRUIT = "Recruit";

	private const string FORT = "Fort";
	private const string GATE = "Gate";

	private const string LEVELTWO = "Level 2";
	private const string LEVELTHREE = "Level 3";

	public const string NONE = "None";
	public const string KNIGHT = "Knight";
	public const string SAMURAI = "Samurai";
	public const string CHINESE = "Chinese";
	public const string VIKING = "Viking";

	private const int SAVE = 0;
	private const int LOAD = 1;

	private List<battleRollItems> battleRollActions = new List<battleRollItems>();
	private List<string> moveRollActions = new List<string>();
	private List<string> fortifyRollActions = new List<string>();
	private List<string> recruitRollActions = new List<string>();
	private bool battleActive = false;
	private bool fortifyActive = false;
	private bool recruitActive = false;
	private bool actionsUsable = false;
	private bool roundEnded = false;

	private int roundCount = 0;
	private int playerSiegeConfirmCount = 0;

	private List<string> messageList = new List<string>();

	struct battleRollItems
	{
		public faction attackingFaction;
		public faction defendingFaction;
		public node nodeUnderAttack;
	}

	// Start is called before the first frame update
	void Start()
    {
		scrollableView.verticalNormalizedPosition = 1f;
		
		factionList = new Dictionary<string, faction>();
		factionList.Add(KNIGHT, new faction(knightHomeBase, false, KNIGHT, new SerializableColor(Color.green)));
		factionList.Add(SAMURAI, new faction(samuraiHomeBase, false, SAMURAI, new SerializableColor(Color.blue)));
		factionList.Add(CHINESE, new faction(chineseHomeBase, false, CHINESE, new SerializableColor(Color.yellow)));
		factionList.Add(VIKING, new faction(vikingHomeBase, true, VIKING, new SerializableColor(Color.red)));
	}
	
    // Update is called once per frame
    void Update()
    {
		if (Input.GetKeyDown(KeyCode.F1)) {
			Debug.Log("Faction INFO:");
			Debug.Log("Knights: Req: " + factionList[KNIGHT].requistionAvaliable + " Level two bots: " + factionList[KNIGHT].levelTwoBotsAvailable + " Level Three bots: " + factionList[KNIGHT].levelThreeBotsAvailable);
			Debug.Log("Samurai: Req: " + factionList[SAMURAI].requistionAvaliable + " Level two bots: " + factionList[SAMURAI].levelTwoBotsAvailable + " Level Three bots: " + factionList[SAMURAI].levelThreeBotsAvailable);
			Debug.Log("Chinese: Req: " + factionList[CHINESE].requistionAvaliable + " Level two bots: " + factionList[CHINESE].levelTwoBotsAvailable + " Level Three bots: " + factionList[CHINESE].levelThreeBotsAvailable);
		}

		if (moveRollActions.Count == 0 &&
			battleRollActions.Count == 0 &&
			fortifyRollActions.Count == 0 &&
			recruitRollActions.Count == 0)
		{
			actionsUsable = false;

			if (roundEnded) {
				sendMessageToLog("--------- ROUND COMPLETE: " + roundCount);
				roundEnded = false;

				saveGameButton.interactable = true;
				loadGameButton.interactable = true;
				playerMoveButton.interactable = true;
				playerRecruitButton.interactable = true;
				playerFortifyButton.interactable = true;
				playerEndTurnButton.interactable = true;

				playerReq.text = factionList[VIKING].requistionAvaliable.ToString();
				playerLevelTwoBots.text = factionList[VIKING].levelTwoBotsAvailable.ToString();
				playerLevelThreeBots.text = factionList[VIKING].levelThreeBotsAvailable.ToString();
			}
		}

		if (actionsUsable) {
			roundEnded = true;

			if (moveRollActions.Count != 0) {
				handleMoveActionAI(factionList[moveRollActions[0]], returnRandomNodeByFaction(factionList[moveRollActions[0]].getFactionName()));
			}
			else if (!battleActive && battleRollActions.Count != 0) {
				battleActive = true;
				battleRollItems currentBattle = battleRollActions[0];
				if (currentBattle.defendingFaction.getFactionName().Equals(getPlayerFaction())) {
					onPlayerAttackAction(currentBattle.nodeUnderAttack, currentBattle.attackingFaction.getFactionName(), false);
				} else {
					onAttackRollRequired(currentBattle.attackingFaction, currentBattle.defendingFaction, currentBattle.nodeUnderAttack);
				}
			}
			else if (!fortifyActive && !battleActive && fortifyRollActions.Count != 0) {
				fortifyActive = true;
				onFortifyAction(factionList[fortifyRollActions[0]]);
			}
			else if (!fortifyActive && !battleActive && !recruitActive && recruitRollActions.Count != 0) {
				recruitActive = true;
				onRecruitAction(factionList[recruitRollActions[0]]);
			}
		}
	}

	public node returnRandomNodeByFaction(string factionKey) {
		List<node> nodesOwnedByFaction = new List<node>();

		for (int i = 0; i < factionAreas.Length; i++) {
			for (int j = 0; j < factionAreas[i].transform.childCount; j++) {
				GameObject selectedNode = factionAreas[i].transform.GetChild(j).gameObject;
				node extractedNode = selectedNode.GetComponent<node>();
				if (extractedNode.ownedBy.Equals(factionKey)) {
					nodesOwnedByFaction.Add(extractedNode);
				}
			}
		}

		return nodesOwnedByFaction[Random.Range(0, nodesOwnedByFaction.Count - 1)];
	}

    public void rollAIAction()
    {
		roundCount++;
		roundCounter.text = roundCount.ToString();

		foreach (var faction in factionList) {
			faction.Value.requistionAvaliable++;
			faction retrievedFaction = faction.Value;

			if (!retrievedFaction.playerOwned) {
				int rolledResult = Random.Range(0, 4);
				string rollAction = mapRollResultToAction(rolledResult);

				switch (rollAction) {
					case MOVE:
						moveRollActions.Add(faction.Key);
						break;
					case FORTIFY:
						if (retrievedFaction.requistionAvaliable >= 3) {
							fortifyRollActions.Add(faction.Key);
						}
						else {
							moveRollActions.Add(faction.Key);
						}
						break;
					case RECRUIT:
						if (retrievedFaction.requistionAvaliable >= 1) {
							recruitRollActions.Add(faction.Key);
						}
						else {
							moveRollActions.Add(faction.Key);
						}
						break;
				}
			}
		}

		actionsUsable = true;
	}

	public void handleMoveActionAI(faction currentFaction, node currentNode) {
		node nextNode = currentNode.navigateToNextNode();

		if (nextNode.ownedBy.Equals(NONE)) {
			nextNode.setOwnership(currentFaction.getFactionName());
			sendMessageToLog(" Move action by: " + currentFaction.getFactionNameUI());
			nextNode.gameObject.GetComponent<SpriteRenderer>().color = currentFaction.getFactionColor().Color;
			moveRollActions.RemoveAt(0);
			return;
		}
		else {
			if (!nextNode.ownedBy.Equals(currentFaction.getFactionName())) {
				onAttackRollQueue(mapFactionBasedOnName(currentNode.ownedBy), mapFactionBasedOnName(nextNode.ownedBy), nextNode);
				moveRollActions.RemoveAt(0);
				return;
			}
			else {
				handleMoveActionAI(currentFaction, nextNode);
				return;
			}
		}
	}

	private string mapRollResultToAction(int rolledResult)
    {
        switch (rolledResult)
        {
            case 0:
                return MOVE;
            case 1:
                return MOVE;
            case 2:
                return FORTIFY;
			case 3:
				return RECRUIT;
        }

        return MOVE;
    }

	private string mapRollResultToStruct(int rolledResult)
	{
		switch (rolledResult)
		{
			case 0:
				return FORT;
			case 1:
				return GATE;
		}

		return FORT;
	}

	private string mapRollResultToBotLevel(int rolledResult)
	{
		switch (rolledResult)
		{
			case 0:
				return LEVELTWO;
			case 1:
				return LEVELTHREE;
		}

		return LEVELTWO;
	}

	public void onFortifyAction(faction currentFaction)
    {
		node selectedFortifyNode = returnRandomNodeByFaction(currentFaction.getFactionName());
		int structResult = Random.Range(1, 3);
		bool nodeMaxedOut = false;

		int maxReq = currentFaction.requistionAvaliable;

		if (currentFaction.requistionAvaliable > 12) {
			maxReq = 4;
		} else if (currentFaction.requistionAvaliable > 9) {
			maxReq = 3;
		} else if (currentFaction.requistionAvaliable > 6) {
			maxReq = 2;
		} else {
			maxReq = 1;
		}

		int reqResult = Random.Range(1, maxReq);
		
		for (int i = 0; i < reqResult; i++) {
			switch (structResult) {
				case 1:
					if (!selectedFortifyNode.isFortLevelMax()) {
						selectedFortifyNode.upgradeFort();
						currentFaction.requistionAvaliable = currentFaction.requistionAvaliable - 3;
					}
					else if (!selectedFortifyNode.isGateLevelMax()) {
						selectedFortifyNode.upgradeGate();
						currentFaction.requistionAvaliable = currentFaction.requistionAvaliable - 3;
					}
					else {
						nodeMaxedOut = true;
					}
					break;
				case 2:
					if (!selectedFortifyNode.isGateLevelMax()) {
						selectedFortifyNode.upgradeGate();
						currentFaction.requistionAvaliable = currentFaction.requistionAvaliable - 3;
					}
					else if (!selectedFortifyNode.isFortLevelMax()) {
						selectedFortifyNode.upgradeFort();
						currentFaction.requistionAvaliable = currentFaction.requistionAvaliable - 3;
					}
					else {
						nodeMaxedOut = true;
					}
				break;
			}
		}

		if (!nodeMaxedOut) {
			sendMessageToLog(" Fortify action by " + currentFaction.getFactionNameUI() + " completed");
		} else {
			sendMessageToLog(" attempted fortify by " + currentFaction.getFactionNameUI() + " but at max fort/gate level!");
		}

		fortifyActive = false;
		fortifyRollActions.RemoveAt(0);
	}

	private int mapReqResultToCost(int reqResult) {
		if (reqResult == 4) {
			return 12;
		} else if (reqResult == 3) {
			return 9;
		} else if (reqResult == 2) {
			return 6;
		} else {
			return 3;
		}
	}

	public string onFortifyRollStructType()
	{
		int structResult = Random.Range(1, 3);

		string structString = mapRollResultToStruct(structResult);

		return structString;
	}

	public void onRecruitAction(faction currentFaction)
    {
		if (currentFaction.requistionAvaliable > 0)
		{
			rollRecruitBotLevels(currentFaction);
		}

		sendMessageToLog(" Recruit Action by " + currentFaction.getFactionNameUI() + " Completed");
		recruitActive = false;
		recruitRollActions.RemoveAt(0);
	}

	public void rollRecruitBotLevels(faction currentFaction)
	{
		int botLevelResult = Random.Range(1, 3);

		string botLevelString = mapRollResultToBotLevel(botLevelResult);

		if (botLevelString == LEVELTHREE && currentFaction.requistionAvaliable >= 3)
		{
			currentFaction.levelThreeBotsAvailable++;
			currentFaction.requistionAvaliable -= 3;
		}
		else
		{
			currentFaction.levelTwoBotsAvailable++;
			currentFaction.requistionAvaliable -= 1;
		}
	}

	public void onAttackRollQueue(faction attacker, faction defender, node nodeUnderAttack) {

		Debug.Log("Queueing up battle...");
		Debug.Log("QUEUED BATTLE, ATTACKER: " + attacker.getFactionNameUI() + " DEFENDER:" + defender.getFactionNameUI());

		battleRollItems newBattle = new battleRollItems();
		newBattle.attackingFaction = attacker;
		newBattle.defendingFaction = defender;
		newBattle.nodeUnderAttack = nodeUnderAttack;

		battleRollActions.Add(newBattle);
	}

	public void onAttackRollRequired(faction attacker, faction defender, node nodeUnderAttack)
	{
		if (attacker != null && defender != null)
		{
			sendMessageToLog("AI BATTLE, DEFENDER: " + defender.getFactionNameUI() + " ATTACKER: " + attacker.getFactionNameUI());
			sendMessageToLog("Node Under Attack: " + nodeUnderAttack.areaName);

			int[] defenderBotLevels = randomizeBotLevels(attacker);
			int[] attackerBotLevels = randomizeBotLevels(defender);

			int maxDefenderRoll = 0;
			int maxAttackerRoll = 0;

			if (nodeUnderAttack.isFortified()) {
				//2-part battle dominion -> siege
				int firstStageRollDefender = mapBotLevelToValue(defenderBotLevels[0]) +
					mapBotLevelToValue(defenderBotLevels[1]) +
					mapBotLevelToValue(defenderBotLevels[2]) +
					mapBotLevelToValue(defenderBotLevels[3]) + 5;

				int firstStageRollAttacker = mapBotLevelToValue(attackerBotLevels[0]) +
					mapBotLevelToValue(attackerBotLevels[1]) +
					mapBotLevelToValue(attackerBotLevels[2]) +
					mapBotLevelToValue(attackerBotLevels[3]) + 5;

				if (firstStageRollAttacker > firstStageRollDefender) {
					maxDefenderRoll = mapBotLevelToValue(defenderBotLevels[0]) +
						mapBotLevelToValue(defenderBotLevels[1]) +
						mapBotLevelToValue(defenderBotLevels[2]) +
						mapBotLevelToValue(defenderBotLevels[3]);

					maxAttackerRoll = mapBotLevelToValue(attackerBotLevels[0]) +
						mapBotLevelToValue(attackerBotLevels[1]) +
						mapBotLevelToValue(attackerBotLevels[2]) +
						mapBotLevelToValue(attackerBotLevels[3]);

					//Extra Ticket buying 
					//-------------------------------------
					int maxTicketRoll = 1;

					if (attacker.requistionAvaliable > 7) {
						maxTicketRoll = 8;
					} else {
						maxTicketRoll = attacker.requistionAvaliable;
					}

					int resultingTicketRoll = Random.Range(1, maxTicketRoll + 1);

					attacker.requistionAvaliable -= (resultingTicketRoll - 1);

					maxAttackerRoll += mapTicketRollToValue(resultingTicketRoll);
					//-------------------------------------

					//Ram Health buying (Only level 2 and 3 forts)
					//-------------------------------------
					if (nodeUnderAttack.isFortMoreThanLevel1()) {
						int maxRamRoll = 1;

						if (attacker.requistionAvaliable > 6) {
							maxRamRoll = 4;
						}
						else {
							if (attacker.requistionAvaliable <= 1) {
								maxRamRoll = 1;
							} else if (attacker.requistionAvaliable <= 3) {
								maxRamRoll = 2;
							} else if (attacker.requistionAvaliable <= 5) {
								maxRamRoll = 3;
							} else {
								maxRamRoll = 4;
							}
						}

						int resultingRamRoll = Random.Range(1, maxRamRoll + 1);

						attacker.requistionAvaliable -= mapRamRollResultToReq(resultingRamRoll);

						maxAttackerRoll += mapRamRollToValue(resultingRamRoll);
					}
					//-------------------------------------

					//Pike damage buying
					//-------------------------------------
					int maxPikeRoll = 1;

					if (attacker.requistionAvaliable > 6) {
						maxPikeRoll = 3;
					}
					else {
						if (attacker.requistionAvaliable <= 2) {
							maxPikeRoll = 1;
						}
						else if (attacker.requistionAvaliable <= 5) {
							maxPikeRoll = 2;
						}
						else {
							maxPikeRoll = 3;
						}
					}

					int resultingPikeDamageRoll = Random.Range(1, maxPikeRoll + 1);

					attacker.requistionAvaliable -= mapPikeRollResultToReq(resultingPikeDamageRoll);

					maxAttackerRoll += mapPikeRollToValue(resultingPikeDamageRoll);
					//-------------------------------------

					//Defender bonuses
					maxDefenderRoll += nodeUnderAttack.calculateDefenderBonus();
				}
				else {
					maxDefenderRoll = 100;
					maxAttackerRoll = 1;
				}

			} else if (nodeUnderAttack.nearbyNodesFortified()) {
				//just dominion
				maxDefenderRoll = mapBotLevelToValue(defenderBotLevels[0]) +
					mapBotLevelToValue(defenderBotLevels[1]) +
					mapBotLevelToValue(defenderBotLevels[2]) +
					mapBotLevelToValue(defenderBotLevels[3]) + 5;

				maxAttackerRoll = mapBotLevelToValue(attackerBotLevels[0]) +
					mapBotLevelToValue(attackerBotLevels[1]) +
					mapBotLevelToValue(attackerBotLevels[2]) +
					mapBotLevelToValue(attackerBotLevels[3]) + 5;
			}
			else {
				//skrimish
				maxDefenderRoll = mapBotLevelToValue(defenderBotLevels[0]) +
					mapBotLevelToValue(defenderBotLevels[1]) +
					mapBotLevelToValue(defenderBotLevels[2]) +
					mapBotLevelToValue(defenderBotLevels[3]);

				maxAttackerRoll = mapBotLevelToValue(attackerBotLevels[0]) +
					mapBotLevelToValue(attackerBotLevels[1]) +
					mapBotLevelToValue(attackerBotLevels[2]) +
					mapBotLevelToValue(attackerBotLevels[3]);
			}

			int defenderFinalResult = Random.Range(1, maxDefenderRoll + 1);
			int attackerFinalResult = Random.Range(1, maxAttackerRoll + 1);

			if (defenderFinalResult > attackerFinalResult) {
				Debug.Log("Defender won");
				sendMessageToLog("Defender: " + defender.getFactionNameUI() + " won!");

				defender.factionWin();
				attacker.factionLoss();
			}
			else {
				Debug.Log("Attacker won");
				nodeUnderAttack.ownedBy = attacker.getFactionName();
				nodeUnderAttack.gameObject.GetComponent<SpriteRenderer>().color = attacker.getFactionColor().Color;

				sendMessageToLog("Attacker: " + attacker.getFactionNameUI() + " won!");

				attacker.factionWin();
				defender.factionLoss();
			}

			battleActive = false;
			battleRollActions.RemoveAt(0);
		}
		else
		{
			saveGameButton.interactable = false;
			loadGameButton.interactable = false;

			attackActionManualPanel.SetActive(true);

			int botLevelResultSlot1 = Random.Range(1, 4);
			int botLevelResultSlot2 = Random.Range(1, 4);
			int botLevelResultSlot3 = Random.Range(1, 4);
			int botLevelResultSlot4 = Random.Range(1, 4);

			attackActionManualSlotOneAILevel.text = botLevelResultSlot1.ToString();
			attackActionManualSlotTwoAILevel.text = botLevelResultSlot2.ToString();
			attackActionManualSlotThreeAILevel.text = botLevelResultSlot3.ToString();
			attackActionManualSlotFourAILevel.text = botLevelResultSlot4.ToString();

			attackActionManualMapName.text = nodeUnderAttack.areaName;
			attackActionManualFactionName.text = attacker.getFactionNameUI();
		}
	}

	private int[] randomizeBotLevels(faction factionWithBots) {
		int[] botLevels = new int[4];

		for (int i = 0; i < 4; i++) {
			int maxBotLevel = 1;

			if (factionWithBots.hasLevelTwoBots()) {
				maxBotLevel = 2;
			}
			else if (factionWithBots.hasLevelThreeBots()) {
				maxBotLevel = 3;
			}

			int botLevelToUse = Random.Range(1, maxBotLevel + 1);

			if (botLevelToUse == 2) {
				factionWithBots.incrementLevelTwoBotInUse();
			} else if (botLevelToUse == 3) {
				factionWithBots.incrementLevelThreeBotInUse();
			}

			botLevels[i] = botLevelToUse;
		}

		return botLevels;
	}

	public void onPlayerLoss() {
		attackActionManualFullScreenPanel.SetActive(false);
		attackActionAiSiegeDecisionsPanel.SetActive(false);
		attackActionPlayerSiegeDecisionsPanel.SetActive(false);

		sendMessageToLog(" Attack action: " + attackingFaction.getFactionNameUI() + " won!");

		playerBotLevelSlot1.text = "";
		playerBotLevelSlot2.text = "";
		playerBotLevelSlot3.text = "";
		playerBotLevelSlot4.text = "";

		playerExtraTickets.value = 0;
		playerPikeDamage.value = 0;
		playerRamHealth.value = 0;

		playerBotLevelSlot1.interactable = true;
		playerBotLevelSlot2.interactable = true;
		playerBotLevelSlot3.interactable = true;
		playerBotLevelSlot4.interactable = true;

		playerExtraTickets.interactable = true;
		playerRamHealth.interactable = true;
		playerPikeDamage.interactable = true;

		attackActionConfirmBotLevel.interactable = true;
		attackActionPlayerWin.interactable = false;
		attackActionPlayerLoss.interactable = false;

		if (!isPlayerAttacking) {
			battleActive = false;
			battleRollActions.RemoveAt(0);

			nodeUnderAttack.ownedBy = attackingFaction.getFactionName();
			nodeUnderAttack.gameObject.GetComponent<SpriteRenderer>().color = attackingFaction.getFactionColor().Color;
			nodeUnderAttack.resetNodeGateFortLevel();

			defendingFaction.factionLoss();
			attackingFaction.factionWin();

			sendMessageToLog(" Defense action: Player Lost vs " + defendingFaction.getFactionNameUI());
		} else {
			nodeUnderAttack.ownedBy = defendingFaction.getFactionName();
			nodeUnderAttack.gameObject.GetComponent<SpriteRenderer>().color = defendingFaction.getFactionColor().Color;

			attackingFaction.factionLoss();
			defendingFaction.factionWin();

			sendMessageToLog(" Defense action: Player Lost vs " + attackingFaction.getFactionNameUI());

			onPlayerMoveFinished();
		}

		nodeUnderAttack = null;
		attackingFaction = null;
		defendingFaction = null;
		isAIAttackingInSiege = false;
	}

	public void onPlayerWin()
	{
		attackActionManualFullScreenPanel.SetActive(false);
		attackActionAiSiegeDecisionsPanel.SetActive(false);
		attackActionPlayerSiegeDecisionsPanel.SetActive(false);

		playerBotLevelSlot1.text = "";
		playerBotLevelSlot2.text = "";
		playerBotLevelSlot3.text = "";
		playerBotLevelSlot4.text = "";

		playerExtraTickets.value = 0;
		playerPikeDamage.value = 0;
		playerRamHealth.value = 0;

		playerBotLevelSlot1.interactable = true;
		playerBotLevelSlot2.interactable = true;
		playerBotLevelSlot3.interactable = true;
		playerBotLevelSlot4.interactable = true;

		playerExtraTickets.interactable = true;
		playerRamHealth.interactable = true;
		playerPikeDamage.interactable = true;

		attackActionConfirmBotLevel.interactable = true;
		attackActionPlayerWin.interactable = false;
		attackActionPlayerLoss.interactable = false;

		if (!isPlayerAttacking) {
			nodeUnderAttack.ownedBy = defendingFaction.getFactionName();
			nodeUnderAttack.gameObject.GetComponent<SpriteRenderer>().color = defendingFaction.getFactionColor().Color;

			attackingFaction.factionLoss();
			defendingFaction.factionWin();

			sendMessageToLog(" Attack action: Player Won vs " + attackingFaction.getFactionNameUI());

			battleActive = false;
			battleRollActions.RemoveAt(0);
		}
		else {
			nodeUnderAttack.ownedBy = attackingFaction.getFactionName();
			nodeUnderAttack.gameObject.GetComponent<SpriteRenderer>().color = attackingFaction.getFactionColor().Color;
			nodeUnderAttack.resetNodeGateFortLevel();

			attackingFaction.factionWin();
			defendingFaction.factionLoss();

			sendMessageToLog(" Defense action: Player Won vs " + defendingFaction.getFactionNameUI());

			onPlayerMoveFinished();
		}

		nodeUnderAttack = null;
		attackingFaction = null;
		defendingFaction = null;
		isAIAttackingInSiege = false;
	}

	public void saveWorldMap()
	{
		nodeInfoSave save = new nodeInfoSave();

		for (int i = 0; i < factionAreas.Length; i++) {
			for (int j = 0; j < factionAreas[i].transform.childCount; j++) {
				GameObject selectedNode = factionAreas[i].transform.GetChild(j).gameObject;
				node extractedNode = selectedNode.GetComponent<node>();
				save.nodeOwnershipList.Add(extractedNode.ownedBy);
				save.nodeFortLevelList.Add(extractedNode.getCurrentFortLevel());
				save.nodeGateLevelList.Add(extractedNode.getCurrentGateLevel());
			}
		}

		save.roundCount = roundCount;
		save.factionList = factionList;

		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create(Application.persistentDataPath + "/gamesave.save");
		bf.Serialize(file, save);
		file.Close();
	}

	public void loadWorldMap() {
		if (File.Exists(Application.persistentDataPath + "/gamesave.save")) {
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);
			nodeInfoSave save = (nodeInfoSave)bf.Deserialize(file);
			file.Close();

			int currentNode = 0;

			for (int i = 0; i < factionAreas.Length; i++) {
				for (int j = 0; j < factionAreas[i].transform.childCount; j++) {
					GameObject selectedNode = factionAreas[i].transform.GetChild(j).gameObject;
					node extractedNode = selectedNode.GetComponent<node>();
					if (!save.nodeOwnershipList[currentNode].Equals(NONE)) {
						extractedNode.ownedBy = save.nodeOwnershipList[currentNode];
						extractedNode.gameObject.GetComponent<SpriteRenderer>().color = mapFactionBasedOnName(save.nodeOwnershipList[currentNode]).getFactionColor().Color;
						extractedNode.updateNodeFortificationLevel(save.nodeFortLevelList[currentNode], save.nodeGateLevelList[currentNode]);
					}
					currentNode++;
				}
			}

			roundCount = save.roundCount;
			factionList = save.factionList;

			playerReq.text = save.factionList[getPlayerFaction()].requistionAvaliable.ToString();
			playerLevelTwoBots.text = save.factionList[getPlayerFaction()].levelTwoBotsAvailable.ToString();
			playerLevelThreeBots.text = save.factionList[getPlayerFaction()].levelThreeBotsAvailable.ToString();

			roundCounter.text = roundCount.ToString();
		}
	}

	public string getPlayerFaction()
	{
		return VIKING;
	}

	public void sendMessageToLog(string message)
	{
		messageList.Add(message);
		GameObject newLine = Instantiate(aiActionTextPrefab, actionOutput.transform);
		newLine.GetComponent<Text>().text = message;
	}

	public void showLog()
	{
		logPanel.SetActive(true);
	}

	public void hideLog()
	{
		logPanel.SetActive(false);
	}

	public void showSaveCtaConfirmationPanel()
	{
		ctaConfirmPanel.SetActive(true);
		ctaText.text = "SAVE";
		actionType = SAVE;
	}

	public void showLoadCtaConfirmationPanel()
	{
		ctaConfirmPanel.SetActive(true);
		ctaText.text = "LOAD";
		actionType = LOAD;
	}

	public void confirmAction()
	{
		ctaConfirmPanel.SetActive(false);
		if (actionType == SAVE)
		{
			saveWorldMap();
		}
		else if (actionType == LOAD)
		{
			loadWorldMap();
		}
	}

	public void cancelAction()
	{
		ctaConfirmPanel.SetActive(false);
	}

	private void rollCoordinates(Text xTextTarget, Text yTextTarget)
	{
		int directionX = Random.Range(1, 9);
		int directionY = Random.Range(1, 9);

		xTextTarget.text = directionX.ToString();
		yTextTarget.text = directionY.ToString();
	}

	private int mapBotLevelToValue(int botLevel)
	{
		if (botLevel == 1){
			return 5;
		}
		else if (botLevel == 2){
			return 15;
		}
		else if (botLevel == 3){
			return 25;
		}

		return 0;
	}

	private int mapRamRollResultToReq(int ramRollResult) {
		if (ramRollResult == 1) {
			return 0;
		}
		else if (ramRollResult == 2) {
			return 2;
		}
		else if (ramRollResult == 3) {
			return 4;
		}
		else if (ramRollResult == 4) {
			return 6;
		}

		return 0;
	}

	private string mapRamRollResultToString(int ramRollResult) {
		if (ramRollResult == 1) {
			return "Low Ram HP";
		}
		else if (ramRollResult == 2) {
			return "Half Ram HP";
		}
		else if (ramRollResult == 3) {
			return "Medium Ram HP";
		}
		else if (ramRollResult == 4) {
			return "Full Ram HP";
		}

		return "N/A";
	}

	private int mapRamRollResultToReqPlayer(int ramRollResult) {
		if (ramRollResult == 0) {
			return 0;
		}
		else if (ramRollResult == 1) {
			return 3;
		}
		else if (ramRollResult == 2) {
			return 6;
		}
		else if (ramRollResult == 3) {
			return 9;
		}

		return 0;
	}

	private int mapRamRollToValue(int ramRollResult) {
		if (ramRollResult == 1) {
			return -5;
		}
		else if (ramRollResult == 2) {
			return 0;
		}
		else if (ramRollResult == 3) {
			return 5;
		}
		else if (ramRollResult == 4) {
			return 10;
		}

		return 0;
	}

	private int mapPikeRollResultToReq(int pikeRollResult) {
		if (pikeRollResult == 1) {
			return 0;
		}
		else if (pikeRollResult == 2) {
			return 3;
		}
		else if (pikeRollResult == 3) {
			return 6;
		}

		return 0;
	}

	private string mapPikeRollResultToString(int pikeRollResult) {
		if (pikeRollResult == 1) {
			return "Low Pike DMG";
		}
		else if (pikeRollResult == 2) {
			return "Med Pike DMG";
		}
		else if (pikeRollResult == 3) {
			return "High Pike DMG";
		}

		return "N/A";
	}

	private int mapPikeRollResultToReqPlayer(int pikeRollResult) {
		if (pikeRollResult == 0) {
			return 0;
		}
		else if (pikeRollResult == 1) {
			return 3;
		}
		else if (pikeRollResult == 2) {
			return 6;
		}

		return 0;
	}

	private int mapPikeRollToValue(int pikeRollResult) {
		if (pikeRollResult == 1) {
			return 0;
		}
		else if (pikeRollResult == 2) {
			return 5;
		}
		else if (pikeRollResult == 3) {
			return 10;
		}

		return 0;
	}

	private int mapTicketRollToValue(int resultingTicketRoll) {
		if (resultingTicketRoll == 1) {
			return -12;
		}
		else if (resultingTicketRoll == 2) {
			return -10;
		}
		else if (resultingTicketRoll == 3) {
			return -8;
		}
		else if (resultingTicketRoll == 4) {
			return -6;
		}
		else if (resultingTicketRoll == 5) {
			return 0;
		}
		else if (resultingTicketRoll == 6) {
			return 6;
		}
		else if (resultingTicketRoll == 7) {
			return 8;
		}
		else if (resultingTicketRoll == 8) {
			return 10;
		}

		return 0;
	}

	private string mapTicketRollToString(int resultingTicketRoll) {
		if (resultingTicketRoll == 1) {
			return "4 Tickets";
		}
		else if (resultingTicketRoll == 2) {
			return "8 Tickets";
		}
		else if (resultingTicketRoll == 3) {
			return "12 Tickets";
		}
		else if (resultingTicketRoll == 4) {
			return "16 Tickets";
		}
		else if (resultingTicketRoll == 5) {
			return "20 Tickets";
		}
		else if (resultingTicketRoll == 6) {
			return "24 Tickets";
		}
		else if (resultingTicketRoll == 7) {
			return "28 Tickets";
		}
		else if (resultingTicketRoll == 8) {
			return "32 Tickets";
		}

		return "N/A";
	}

	private faction mapFactionBasedOnName(string factionName) {
		return factionList[factionName];
	}

	public bool isPlayersTurn() {
		return !actionsUsable;
	}

	public bool isPlayerMoving() {
		return playerMoving;
	}

	public bool isPlayerFortifying() {
		return playerFortifying;
	}

	public void onPlayerMoveAction() {
		playerMoving = true;

		saveGameButton.interactable = false;
		loadGameButton.interactable = false;
		playerMoveButton.interactable = false;
		playerRecruitButton.interactable = false;
		playerFortifyButton.interactable = false;
		playerEndTurnButton.interactable = false;
	}

	public void onPlayerFortifyAction() {
		playerFortifying = true;

		saveGameButton.interactable = false;
		loadGameButton.interactable = false;
		playerMoveButton.interactable = false;
		playerRecruitButton.interactable = false;
		playerFortifyButton.interactable = false;
		playerEndTurnButton.interactable = false;
	}

	public void onPlayerNodeSelected(node fortifyingNode) {
		this.fortifyingNode = fortifyingNode;

		for (int j = 0; j < gateLevels.Length; j++) {
			ColorBlock colorBlock = new ColorBlock();
			gateLevels[j].interactable = false;
			colorBlock.normalColor = Color.white;
			colorBlock.highlightedColor = Color.white;
			colorBlock.selectedColor = Color.white;
			colorBlock.pressedColor = Color.grey;
			colorBlock.disabledColor = Color.grey;
			colorBlock.colorMultiplier = 1;

			gateLevels[j].colors = colorBlock;
		}

		for (int j = 0; j < fortLevels.Length; j++) {
			ColorBlock colorBlock = new ColorBlock();
			fortLevels[j].interactable = false;
			colorBlock.normalColor = Color.white;
			colorBlock.highlightedColor = Color.white;
			colorBlock.selectedColor = Color.white;
			colorBlock.pressedColor = Color.grey;
			colorBlock.disabledColor = Color.grey;
			colorBlock.colorMultiplier = 1;

			fortLevels[j].colors = colorBlock;
		}

		playerReqFortify.text = factionList[VIKING].requistionAvaliable.ToString();

		int levelsAvailable = factionList[VIKING].requistionAvaliable / 3;

		if (levelsAvailable > 4) {
			levelsAvailable = 4;
		}

		for (int j = fortifyingNode.getCurrentGateLevel(); j < levelsAvailable; j++) {
			if (j > 3) {
				break;
			}
			gateLevels[j].interactable = true;
		}

		for (int j = fortifyingNode.getCurrentFortLevel(); j < levelsAvailable; j++) {
			if (j > 3) {
				break;
			}
			fortLevels[j].interactable = true;
		}

		for (int i = 0; i < fortifyingNode.getCurrentGateLevel(); i++) {
			gateLevels[i].interactable = false;

			ColorBlock colorBlock = new ColorBlock();
			colorBlock.normalColor = Color.white;
			colorBlock.highlightedColor = Color.white;
			colorBlock.selectedColor = Color.white;
			colorBlock.pressedColor = Color.grey;
			colorBlock.disabledColor = Color.green;
			colorBlock.colorMultiplier = 1;

			gateLevels[i].colors = colorBlock;
		}

		for (int i = 0; i < fortifyingNode.getCurrentFortLevel(); i++) {
			fortLevels[i].interactable = false;

			ColorBlock colorBlock = new ColorBlock();
			colorBlock.normalColor = Color.white;
			colorBlock.highlightedColor = Color.white;
			colorBlock.selectedColor = Color.white;
			colorBlock.pressedColor = Color.grey;
			colorBlock.disabledColor = Color.green;
			colorBlock.colorMultiplier = 1;

			fortLevels[i].colors = colorBlock;
		}

		ctaFortifyActionPanel.SetActive(true);
	}

	public void onPlayerMoveFinished() {
		playerMoving = false;
		rollAIAction();
	}

	public void onPlayerAttackAction(node nodeUnderAttack, string factionAttacking, bool isPlayerAttacking) {
		defendingFaction = mapFactionBasedOnName(nodeUnderAttack.ownedBy);
		attackingFaction = mapFactionBasedOnName(factionAttacking);
		this.nodeUnderAttack = nodeUnderAttack;

		attackActionManualFullScreenPanel.SetActive(true);

		int[] enemyBotsInUse = new int[4];

		if (!isPlayerAttacking) {
			enemyBotsInUse = randomizeBotLevels(attackingFaction);
		} else {
			enemyBotsInUse = randomizeBotLevels(defendingFaction);
		}

		attackActionManualPlayerBot2Avaliable.text = factionList[getPlayerFaction()].levelTwoBotsAvailable.ToString();
		attackActionManualPlayerBot3Avaliable.text = factionList[getPlayerFaction()].levelThreeBotsAvailable.ToString();
		attackActionManualPlayerReqAvaliable.text = factionList[getPlayerFaction()].requistionAvaliable.ToString();

		attackActionManualSlotOneAILevel.text = enemyBotsInUse[0].ToString();
		attackActionManualSlotTwoAILevel.text = enemyBotsInUse[1].ToString();
		attackActionManualSlotThreeAILevel.text = enemyBotsInUse[2].ToString();
		attackActionManualSlotFourAILevel.text = enemyBotsInUse[3].ToString();

		attackActionManualMapName.text = nodeUnderAttack.areaName;
		attackActionManualFactionName.text = factionAttacking;
		attackActionManualDefendingFaction.text = factionList[nodeUnderAttack.ownedBy].getFactionNameUI();
		attackFortLevel.text = nodeUnderAttack.getCurrentFortLevel().ToString();
		attackGateLevel.text = nodeUnderAttack.getCurrentGateLevel().ToString();

		if (nodeUnderAttack.isFortified()) {
			if (!isPlayerAttacking) {
				attackActionAiSiegeDecisionsPanel.SetActive(true);

				//Extra Ticket buying 
				//-------------------------------------
				int maxTicketRoll = 1;

				if (attackingFaction.requistionAvaliable > 7) {
					maxTicketRoll = 8;
				}
				else {
					maxTicketRoll = attackingFaction.requistionAvaliable;
				}

				int resultingTicketRoll = Random.Range(1, maxTicketRoll + 1);
				attackingFaction.requistionAvaliable -= (resultingTicketRoll - 1);
				attackActionAiSiegeExtraTickets.text = mapTicketRollToString(resultingTicketRoll);
				//-------------------------------------

				//Ram Health buying (Only level 2 and 3 forts)
				//-------------------------------------
				if (nodeUnderAttack.isFortMoreThanLevel1()) {
					int maxRamRoll = 1;

					if (attackingFaction.requistionAvaliable > 6) {
						maxRamRoll = 4;
					}
					else {
						if (attackingFaction.requistionAvaliable <= 1) {
							maxRamRoll = 1;
						}
						else if (attackingFaction.requistionAvaliable <= 3) {
							maxRamRoll = 2;
						}
						else if (attackingFaction.requistionAvaliable <= 5) {
							maxRamRoll = 3;
						}
						else {
							maxRamRoll = 4;
						}
					}

					int resultingRamRoll = Random.Range(1, maxRamRoll + 1);
					attackingFaction.requistionAvaliable -= mapRamRollResultToReq(resultingRamRoll);
					attackActionAiSiegeRamHealth.text = mapRamRollResultToString(resultingRamRoll);
				} else {
					attackActionAiSiegeRamHealth.text = "N/A";
				}
				//-------------------------------------

				//Pike damage buying
				//-------------------------------------
				int maxPikeRoll = 1;

				if (attackingFaction.requistionAvaliable > 6) {
					maxPikeRoll = 3;
				}
				else {
					if (attackingFaction.requistionAvaliable <= 2) {
						maxPikeRoll = 1;
					}
					else if (attackingFaction.requistionAvaliable <= 5) {
						maxPikeRoll = 2;
					}
					else {
						maxPikeRoll = 3;
					}
				}

				int resultingPikeDamageRoll = Random.Range(1, maxPikeRoll + 1);
				attackingFaction.requistionAvaliable -= mapPikeRollResultToReq(resultingPikeDamageRoll);
				attackActionAiSiegePikeDamage.text = mapPikeRollResultToString(resultingPikeDamageRoll);

				isAIAttackingInSiege = true;
			} else {
				attackActionPlayerSiegeDecisionsPanel.SetActive(true);
			}

			attackActionManualBattleType.text = "Dominion -> Siege";
		} else if (nodeUnderAttack.nearbyNodesFortified()) {
			attackActionManualBattleType.text = "Dominion";
		} else {
			attackActionManualBattleType.text = "Skirmish";
		}

		this.isPlayerAttacking = isPlayerAttacking;
	}

	public void onPlayerRecruitAction() {
		ctaRecruitActionPanel.SetActive(true);

		if (factionList[VIKING].requistionAvaliable >= 3) {
			playerRecruitLevelTwoBotButton.interactable = true;
		}

		if (factionList[VIKING].requistionAvaliable >= 5) {
			playerRecruitLevelThreeBotButton.interactable = true;
		}
	}

	public void onPlayerRecruitLevelTwoBot() {
		factionList[VIKING].requistionAvaliable -= 3;
		factionList[VIKING].levelTwoBotsAvailable++;
		resetRecruitCta();

		rollAIAction();
	}

	public void onPlayerRecruitLevelThreeBot() {
		factionList[VIKING].requistionAvaliable -= 5;
		factionList[VIKING].levelThreeBotsAvailable++;
		resetRecruitCta();

		rollAIAction();
	}

	public void resetRecruitCta() {
		ctaRecruitActionPanel.SetActive(false);

		playerRecruitLevelThreeBotButton.interactable = false;
		playerRecruitLevelTwoBotButton.interactable = false;
	}

	public void resetFortifyCta() {
		playerFortifying = false;
		fortifyingNode = null;

		ctaFortifyActionPanel.SetActive(false);

		saveGameButton.interactable = true;
		loadGameButton.interactable = true;
		playerMoveButton.interactable = true;
		playerRecruitButton.interactable = true;
		playerFortifyButton.interactable = true;
		playerEndTurnButton.interactable = true;
		playerForitfyCancelButton.interactable = true;
		playerForitfyDoneButton.interactable = false;
	}

	public void upgradeFort(int desiredFortLevel) {
		int upgradeDifference = desiredFortLevel - fortifyingNode.getCurrentFortLevel();

		for (int i = 0; i < upgradeDifference; i++) {
			fortifyingNode.upgradeFort();
		}

		int reqUsed = upgradeDifference * 3;
		factionList[VIKING].requistionAvaliable -= reqUsed;

		playerForitfyDoneButton.interactable = true;
		playerForitfyCancelButton.interactable = false;

		onPlayerNodeSelected(fortifyingNode);
	}

	public void upgradeGate(int desiredGateLevel) {
		int upgradeDifference = desiredGateLevel - fortifyingNode.getCurrentGateLevel();

		for (int i = 0; i < upgradeDifference; i++) {
			fortifyingNode.upgradeGate();
		}

		int reqUsed = upgradeDifference * 3;
		factionList[VIKING].requistionAvaliable -= reqUsed;

		playerForitfyDoneButton.interactable = true;
		playerForitfyCancelButton.interactable = false;

		onPlayerNodeSelected(fortifyingNode);
	}

	public void onPlayerFortifyFinished() {
		resetFortifyCta();
		rollAIAction();
	}

	public void onPlayerBotLevelConfirm() {
		playerBotLevelErrorMessage.text = "";

		int[] chosenBotLevels = new int[4];
		int levelTwoBots = 0;
		int levelThreeBots = 0;

		int.TryParse(playerBotLevelSlot1.text, out chosenBotLevels[0]);
		int.TryParse(playerBotLevelSlot2.text, out chosenBotLevels[1]);
		int.TryParse(playerBotLevelSlot3.text, out chosenBotLevels[2]);
		int.TryParse(playerBotLevelSlot4.text, out chosenBotLevels[3]);

		foreach (int botLevel in chosenBotLevels) {
			if (botLevel == 0) {
				playerBotLevelErrorMessage.text = "Error: No bot level specified! Use at least 1";
				return;
			}

			if (botLevel > 3) {
				playerBotLevelErrorMessage.text = "Error: Bot level too high! Use only 1-3";
				return;
			}

			if (botLevel == 2) {
				levelTwoBots++;
			} else if (botLevel == 3) {
				levelThreeBots++;
			}
		}

		if (levelTwoBots > factionList[getPlayerFaction()].levelTwoBotsAvailable) {
			playerBotLevelErrorMessage.text = "Error: You do not have enough level two bots! Lower their level or use level 3";
			return;
		}

		if (levelThreeBots > factionList[getPlayerFaction()].levelThreeBotsAvailable) {
			playerBotLevelErrorMessage.text = "Error: You do not have enough level three bots! Lower their level";
			return;
		}

		for (int i = 0; i < levelTwoBots; i++) {
			factionList[getPlayerFaction()].incrementLevelTwoBotInUse();
		}

		for (int i = 0; i < levelThreeBots; i++) {
			factionList[getPlayerFaction()].incrementLevelThreeBotInUse();
		}

		if (!nodeUnderAttack.isFortified()) {
			attackActionPlayerWin.interactable = true;
			attackActionPlayerLoss.interactable = true;
		} else if (nodeUnderAttack.isFortified() && isAIAttackingInSiege) {
			attackActionPlayerWin.interactable = true;
			attackActionPlayerLoss.interactable = true;
		} else {
			playerExtraTickets.interactable = true;
			attackActionConfirmSiege.interactable = true;
		}
		attackActionConfirmBotLevel.interactable = false;

		playerBotLevelSlot1.interactable = false;
		playerBotLevelSlot2.interactable = false;
		playerBotLevelSlot3.interactable = false;
		playerBotLevelSlot4.interactable = false;
	}

	public void onPlayerSiegeConfirm() {
		playerBotLevelErrorMessage.text = "";

		int playerExtraTicketsNum = 0;
		int playerRamHealthNum = 0;
		int playerPikeDamageNum = 0;
		
		if (playerSiegeConfirmCount == 0) {
			playerExtraTicketsNum = playerExtraTickets.value;

			if (playerExtraTicketsNum > factionList[getPlayerFaction()].requistionAvaliable) {
				playerBotLevelErrorMessage.text = "Error: Not enough req for extra tickets!";
				return;
			}

			factionList[getPlayerFaction()].requistionAvaliable -= playerExtraTicketsNum;
			attackActionManualPlayerReqAvaliable.text = factionList[getPlayerFaction()].requistionAvaliable.ToString();

			playerSiegeConfirmCount++;
			playerExtraTickets.interactable = false;
			playerRamHealth.interactable = true;
			return;
		} else if (playerSiegeConfirmCount == 1) {
			playerRamHealthNum = playerRamHealth.value;

			if (mapRamRollResultToReqPlayer(playerRamHealthNum) > factionList[getPlayerFaction()].requistionAvaliable) {
				playerBotLevelErrorMessage.text = "Error: Not enough req for extra ram health!";
				return;
			}

			factionList[getPlayerFaction()].requistionAvaliable -= mapRamRollResultToReqPlayer(playerRamHealthNum);
			attackActionManualPlayerReqAvaliable.text = factionList[getPlayerFaction()].requistionAvaliable.ToString();

			playerSiegeConfirmCount++;
			playerRamHealth.interactable = false;
			playerPikeDamage.interactable = true;
			return;
		} else if (playerSiegeConfirmCount == 2) {
			playerPikeDamageNum = playerPikeDamage.value;

			if (mapPikeRollResultToReqPlayer(playerPikeDamageNum) > factionList[getPlayerFaction()].requistionAvaliable) {
				playerBotLevelErrorMessage.text = "Error: Not enough req for extra pike damage!";
				return;
			}

			factionList[getPlayerFaction()].requistionAvaliable -= mapPikeRollResultToReqPlayer(playerPikeDamageNum);
			attackActionManualPlayerReqAvaliable.text = factionList[getPlayerFaction()].requistionAvaliable.ToString();

			playerSiegeConfirmCount++;
		}

		playerSiegeConfirmCount = 0;

		attackActionConfirmSiege.interactable = false;
		attackActionPlayerWin.interactable = true;
		attackActionPlayerLoss.interactable = true;

		playerExtraTickets.interactable = false;
		playerRamHealth.interactable = false;
		playerPikeDamage.interactable = false;
	}

	public void populateNodeInfoPanel(string ownedBy, string fortLevel, string gateLevel, string areaName) {
		nodeInfoOwnedBy.text = ownedBy;
		nodeInfoFortLevel.text = fortLevel;
		nodeInfoGateLevel.text = gateLevel;
		nodeInfoAreaName.text = areaName;
	}
}
